package com.itil.s02activity;


import java.util.Scanner;

public class LeapYearIdentifier {
        public static void main(String[] args){
            Scanner input = new Scanner(System.in);
            System.out.println("Enter any Year \n");
            int year = input.nextInt();
            if( year%400 == 0 || year%4 == 0) {
                System.out.println(year + "is a leap year");
            } else {
                System.out.println(year +  "is not a leap year");
            }



        }
}
